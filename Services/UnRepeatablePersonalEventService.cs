﻿using QM_Schedule.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QM_Schedule.Services
{
    public class UnRepeatablePersonalEventService
    {

        static List<UnRepeatablePersonalEvent> UnRepeatablePersonalEvents { get; }
        static ulong NextId = 3;
        static UnRepeatablePersonalEventService()
        {
            UnRepeatablePersonalEvents = new List<UnRepeatablePersonalEvent>
            {
                new UnRepeatablePersonalEvent
                {
                    Id = 1,
                    Date = DateTime.Now,
                    StartTime = new TimeSpan(15,30,00),
                    EndTime = new TimeSpan(16,20,00),
                    Name = "123",
                    PlaceId = 1
                },
                new UnRepeatablePersonalEvent
                {
                    Id = 2,
                    Date = DateTime.Now,
                    StartTime = new TimeSpan(17,20,00),
                    EndTime = new TimeSpan(18,10,00),
                    Name = "qwe123sd",
                    PlaceId = 2                    
                }
            };
        }

        public static List<UnRepeatablePersonalEvent> GetAll() => UnRepeatablePersonalEvents;
        public static UnRepeatablePersonalEvent GetById(ulong Id) =>
            UnRepeatablePersonalEvents.FirstOrDefault(s => s.Id == Id);

        public static void Add(UnRepeatablePersonalEvent unRepeatablePersonalEvent)
        {
            unRepeatablePersonalEvent.Id = NextId++;
            UnRepeatablePersonalEvents.Add(unRepeatablePersonalEvent);
        }

        public static void Update(UnRepeatablePersonalEvent unRepeatablePersonalEvent)
        {
            var index = UnRepeatablePersonalEvents.FindIndex(s => s.Id == unRepeatablePersonalEvent.Id);
            if (index == -1)
                return;
            UnRepeatablePersonalEvents[index] = unRepeatablePersonalEvent;
        }

        public static void Delete(ulong Id)
        {
            var unRepeatablePersonalEvent = GetById(Id);
            if (unRepeatablePersonalEvent is null)
                return;
            UnRepeatablePersonalEvents.Remove(unRepeatablePersonalEvent);
        }
    }
}
