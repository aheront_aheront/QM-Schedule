﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QM_Schedule.Models
{
    public class TempLesson:Lesson
    {
        public DateTime Date { get; set; }
        public ulong LessonToChangeId { get; set; }
    }
}
