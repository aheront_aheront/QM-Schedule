﻿using QM_Schedule.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QM_Schedule.Services
{
    public class TempLessonService
    {

        static List<TempLesson> TempLessons { get; }
        static ulong NextId = 3;
        static TempLessonService()
        {
            TempLessons = new List<TempLesson>
            {
                new TempLesson
                {
                    Id = 1,
                    StartTime = TimeSpan.Zero,
                    EndTime = TimeSpan.Zero,
                    SubjectId = 1,
                    Date = DateTime.Now,
                    LessonToChangeId = 2,
                    PlaceId = 2,
                    Topic = "123asd"
                },
                new TempLesson
                {
                    Id = 2,
                    StartTime = TimeSpan.Zero,
                    EndTime = TimeSpan.Zero,
                    SubjectId = 2,
                    Date = DateTime.Now,
                    LessonToChangeId = 1,
                    PlaceId = 1,
                    Topic = "asd123"
                }
            };
        }

        public static List<TempLesson> GetAll() => TempLessons;
        public static TempLesson GetById(ulong Id) =>
            TempLessons.FirstOrDefault(s => s.Id == Id);

        public static void Add(TempLesson tempLeeson)
        {
            tempLeeson.Id = NextId++;
            TempLessons.Add(tempLeeson);
        }

        public static void Update(TempLesson tempLeeson)
        {
            var index = TempLessons.FindIndex(s => s.Id == tempLeeson.Id);
            if (index == -1)
                return;
            TempLessons[index] = tempLeeson;
        }

        public static void Delete(ulong Id)
        {
            var tempLeeson = GetById(Id);
            if (tempLeeson is null)
                return;
            TempLessons.Remove(tempLeeson);
        }
    }
}
