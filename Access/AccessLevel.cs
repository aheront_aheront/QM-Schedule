﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QM_Schedule.Access
{
    public enum AccessLevel {Pupil, Admin};
    public class AccessValidate
    {
        public static bool IsAdmin(AccessLevel accessLevel) => 
            accessLevel >= AccessLevel.Admin;
    }
}
