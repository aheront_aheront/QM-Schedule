﻿using QM_Schedule.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QM_Schedule.Services
{
    public class ScheduleService
    {
        public static Schedule BuildSchedule()
        {
            Schedule schedule = new Schedule
            {
                DaySchedules = DayScheduleService.GetAll(),
                Grades = GradeService.GetAll(),
                GradeSchedules = GradeScheduleService.GetAll(),
                Lessons = LessonService.GetAll(),
                PersonalScheduleEvents = PersonalScheduleEventService.GetAll(),
                Places = PlaceService.GetAll(),
                Users = UserService.GetAll(),
                RepeatablePersonalEvents = RepeatablePersonalEventService.GetAll(),
                Subjects = SubjectService.GetAll(),
                TempLessons = TempLessonService.GetAll(),
                UnRepeatablePersonalEvents = UnRepeatablePersonalEventService.GetAll()
            };
            
            return schedule;
        }
        public static Schedule GetAll() => BuildSchedule();

        public static Schedule GetByUserId(ulong UserId)
        { 












        return null;
        }
        
    }
}
