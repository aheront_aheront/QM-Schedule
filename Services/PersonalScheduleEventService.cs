﻿using QM_Schedule.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QM_Schedule.Services
{
    public class PersonalScheduleEventService
    {

        static List<PersonalScheduleEvent> PersonalScheduleEvent { get; }
        static ulong NextId = 3;
        static PersonalScheduleEventService()
        {
            PersonalScheduleEvent = new List<PersonalScheduleEvent>
            {
                new PersonalScheduleEvent
                {
                    Id = 1,
                    DaysOfWeek = new List<DayOfWeek>{ DayOfWeek.Monday, DayOfWeek.Wednesday},
                    PlaceId = 1,
                    Name = "asd",
                    StartTime = TimeSpan.Zero,
                    EndTime = TimeSpan.Zero
                },
                new PersonalScheduleEvent
                {
                    Id = 2,
                    DaysOfWeek = new List<DayOfWeek>{ DayOfWeek.Sunday, DayOfWeek.Saturday},
                    PlaceId = 2,
                    Name = "12312",
                    StartTime = TimeSpan.Zero,
                    EndTime = TimeSpan.Zero
                }
            };
        }

        public static List<PersonalScheduleEvent> GetAll() => PersonalScheduleEvent;
        public static PersonalScheduleEvent GetById(ulong Id) =>
            PersonalScheduleEvent.FirstOrDefault(s => s.Id == Id);

        public static void Add(PersonalScheduleEvent personalScheduleEvent)
        {
            personalScheduleEvent.Id = NextId++;
            PersonalScheduleEvent.Add(personalScheduleEvent);
        }

        public static void Update(PersonalScheduleEvent personalScheduleEvent)
        {
            var index = PersonalScheduleEvent.FindIndex(s => s.Id == personalScheduleEvent.Id);
            if (index == -1)
                return;
            PersonalScheduleEvent[index] = personalScheduleEvent;
        }

        public static void Delete(ulong Id)
        {
            var personalScheduleEvent = GetById(Id);
            if (personalScheduleEvent is null)
                return;
            PersonalScheduleEvent.Remove(personalScheduleEvent);
        }
    }
}
