﻿using QM_Schedule.Access;
using QM_Schedule.Services;
using QM_Schedule.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QM_Schedule.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class GradeScheduleController : ControllerBase
    {
        [HttpPost("{UserId}")]
        public IActionResult Create(ulong UserId, GradeSchedule gradeSchedule)
        {
            if (!AccessValidate.IsAdmin(UserService.GetById(UserId).AccessLevel))
                return BadRequest();

            GradeScheduleService.Add(gradeSchedule);
            return CreatedAtAction(nameof(Create), new { id = gradeSchedule.Id }, gradeSchedule);
        }

        [HttpPut("{id}")]
        public IActionResult Update(ulong id, GradeSchedule gradeSchedule)
        {
            if (id != gradeSchedule.Id)
                return BadRequest();

            var existingGradeSchedule = GradeScheduleService.GetById(id);
            if (existingGradeSchedule is null)
                return NotFound();

            GradeScheduleService.Update(gradeSchedule);

            return NoContent();
        }
        [HttpDelete("{id}")]
        public IActionResult Delete(ulong id)
        {
            var gradeSchedule = GradeScheduleService.GetById(id);

            if (gradeSchedule is null)
                return NotFound();

            GradeScheduleService.Delete(id);

            return NoContent();
        }
    }
}
