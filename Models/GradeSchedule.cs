﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QM_Schedule.Models
{
    public class GradeSchedule
    {
        public ulong Id;
        public ulong GradeId { get; set; }
        public List<ulong> DayScheduleIds { get; set; }
    }
}
