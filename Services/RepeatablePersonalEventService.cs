﻿using QM_Schedule.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QM_Schedule.Services
{
    public class RepeatablePersonalEventService
    {

        static List<RepeatablePersonalEvent> RepeatablePersonalEvents { get; }
        static ulong NextId = 3;
        static RepeatablePersonalEventService()
        {
            RepeatablePersonalEvents = new List<RepeatablePersonalEvent>
            {
                new RepeatablePersonalEvent
                {
                    Id = 1,
                    DaysToRepeat = 1,
                    StartTime = TimeSpan.Zero,
                    EndTime = TimeSpan.Zero,
                    RootEvent = DateTime.Now,
                    Name = "asign",
                    PlaceId = 1
                },
                new RepeatablePersonalEvent
                {
                    Id = 2,
                    DaysToRepeat = 4,
                    StartTime = TimeSpan.Zero,
                    EndTime = TimeSpan.Zero,
                    Name = "Loca",
                    PlaceId = 2,
                    RootEvent = DateTime.Now
                }
            };
        }

        public static List<RepeatablePersonalEvent> GetAll() => RepeatablePersonalEvents;
        public static RepeatablePersonalEvent GetById(ulong Id) =>
            RepeatablePersonalEvents.FirstOrDefault(s => s.Id == Id);

        public static void Add(RepeatablePersonalEvent repeatablePersonalEvent)
        {
            repeatablePersonalEvent.Id = NextId++;
            RepeatablePersonalEvents.Add(repeatablePersonalEvent);
        }

        public static void Update(RepeatablePersonalEvent repeatablePersonalEvent)
        {
            var index = RepeatablePersonalEvents.FindIndex(s => s.Id == repeatablePersonalEvent.Id);
            if (index == -1)
                return;
            RepeatablePersonalEvents[index] = repeatablePersonalEvent;
        }

        public static void Delete(ulong Id)
        {
            var repeatablePersonalEvent = GetById(Id);
            if (repeatablePersonalEvent is null)
                return;
            RepeatablePersonalEvents.Remove(repeatablePersonalEvent);
        }
    }
}
