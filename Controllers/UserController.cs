﻿using QM_Schedule.Access;
using QM_Schedule.Services;
using QM_Schedule.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QM_Schedule.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class UserController : ControllerBase
    {
        [HttpPost("{UserId}")]
        public IActionResult Create(ulong UserId, User user)
        {
            if (!AccessValidate.IsAdmin(UserService.GetById(UserId).AccessLevel))
                return BadRequest();

            UserService.Add(user);
            return CreatedAtAction(nameof(Create), new { id = user.Id }, user);
        }

        [HttpPut("{id}")]
        public IActionResult Update(ulong id, User user)
        {
            if (id != user.Id)
                return BadRequest();

            var existingUser = UserService.GetById(id);
            if (existingUser is null)
                return NotFound();

            UserService.Update(user);

            return NoContent();
        }
        [HttpDelete("{id}")]
        public IActionResult Delete(ulong id)
        {
            var user = UserService.GetById(id);

            if (user is null)
                return NotFound();

            UserService.Delete(id);

            return NoContent();
        }
    }
}
