﻿using QM_Schedule.Access;
using QM_Schedule.Services;
using QM_Schedule.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QM_Schedule.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class PersonalScheduleEventController : ControllerBase
    {
        [HttpPost("{UserId}")]
        public IActionResult Create(ulong UserId, PersonalScheduleEvent personalScheduleEvent)
        {
            if (!AccessValidate.IsAdmin(UserService.GetById(UserId).AccessLevel))
                return BadRequest();

            PersonalScheduleEventService.Add(personalScheduleEvent);
            return CreatedAtAction(nameof(Create), new { id = personalScheduleEvent.Id }, personalScheduleEvent);
        }

        [HttpPut("{id}")]
        public IActionResult Update(ulong id, PersonalScheduleEvent personalScheduleEvent)
        {
            if (id != personalScheduleEvent.Id)
                return BadRequest();

            var existingPersonalScheduleEvent = PersonalScheduleEventService.GetById(id);
            if (existingPersonalScheduleEvent is null)
                return NotFound();

            PersonalScheduleEventService.Update(personalScheduleEvent);

            return NoContent();
        }
        [HttpDelete("{id}")]
        public IActionResult Delete(ulong id)
        {
            var personalScheduleEvent = PersonalScheduleEventService.GetById(id);

            if (personalScheduleEvent is null)
                return NotFound();

            PersonalScheduleEventService.Delete(id);

            return NoContent();
        }
    }
}
