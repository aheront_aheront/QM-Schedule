﻿using QM_Schedule.Access;
using QM_Schedule.Services;
using QM_Schedule.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QM_Schedule.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class DayScheduleController : ControllerBase
    {
        [HttpPost("{UserId}")]
        public IActionResult Create(ulong UserId, DaySchedule daySchedule)
        {
            if (!AccessValidate.IsAdmin(UserService.GetById(UserId).AccessLevel))
                return BadRequest();

            DayScheduleService.Add(daySchedule);
            return CreatedAtAction(nameof(Create), new { id = daySchedule.Id }, daySchedule);
        }

        [HttpPut("{id}")]
        public IActionResult Update(ulong id, DaySchedule daySchedule)
        {
            if (id != daySchedule.Id)
                return BadRequest();

            var existingDaySchedule = DayScheduleService.GetById(id);
            if (existingDaySchedule is null)
                return NotFound();

            DayScheduleService.Update(daySchedule);

            return NoContent();
        }
        [HttpDelete("{id}")]
        public IActionResult Delete(ulong id)
        {
            var daySchedule = DayScheduleService.GetById(id);

            if (daySchedule is null)
                return NotFound();

            DayScheduleService.Delete(id);

            return NoContent();
        }
    }
}
