﻿using QM_Schedule.Access;
using QM_Schedule.Services;
using QM_Schedule.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QM_Schedule.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class SubjectController : ControllerBase
    {
        [HttpPost("{UserId}")]
        public IActionResult Create(ulong UserId, Subject subject)
        {
            if (!AccessValidate.IsAdmin(UserService.GetById(UserId).AccessLevel))
                return BadRequest();

            SubjectService.Add(subject);
            return CreatedAtAction(nameof(Create), new { id = subject.Id }, subject);
        }

        [HttpPut("{id}")]
        public IActionResult Update(ulong id, Subject subject)
        {
            if (id != subject.Id)
                return BadRequest();

            var existingSubject = SubjectService.GetById(id);
            if (existingSubject is null)
                return NotFound();

            SubjectService.Update(subject);

            return NoContent();
        }
        [HttpDelete("{id}")]
        public IActionResult Delete(ulong id)
        {
            var subject = SubjectService.GetById(id);

            if (subject is null)
                return NotFound();

            SubjectService.Delete(id);

            return NoContent();
        }
    }
}
