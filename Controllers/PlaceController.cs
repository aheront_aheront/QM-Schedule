﻿using QM_Schedule.Access;
using QM_Schedule.Services;
using QM_Schedule.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QM_Schedule.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class PlaceController : ControllerBase
    {
        [HttpPost("{UserId}")]
        public IActionResult Create(ulong UserId, Place place)
        {
            if (!AccessValidate.IsAdmin(UserService.GetById(UserId).AccessLevel))
                return BadRequest();

            PlaceService.Add(place);
            return CreatedAtAction(nameof(Create), new { id = place.Id }, place);
        }

        [HttpPut("{id}")]
        public IActionResult Update(ulong id, Place place)
        {
            if (id != place.Id)
                return BadRequest();

            var existingPlace = PlaceService.GetById(id);
            if (existingPlace is null)
                return NotFound();

            PlaceService.Update(place);

            return NoContent();
        }
        [HttpDelete("{id}")]
        public IActionResult Delete(ulong id)
        {
            var place = PlaceService.GetById(id);

            if (place is null)
                return NotFound();

            PlaceService.Delete(id);

            return NoContent();
        }
    }
}
