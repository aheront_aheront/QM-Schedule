﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QM_Schedule.Models
{
    public class Schedule //это  контейнер для ответа
    {
        public List<DaySchedule> DaySchedules { get; set; }
        public List<Grade> Grades { get; set; }
        public List<GradeSchedule> GradeSchedules { get; set; }
        public List<Lesson> Lessons { get; set; }
        public List<PersonalScheduleEvent> PersonalScheduleEvents { get; set; }
        public List<Place> Places { get; set; }
        public List<User> Users { get; set; }
        public List<RepeatablePersonalEvent> RepeatablePersonalEvents { get; set; }        public List<Subject> Subjects { get; set; }        public List<TempLesson> TempLessons { get; set; }        public List<UnRepeatablePersonalEvent> UnRepeatablePersonalEvents { get; set; }    }
}
