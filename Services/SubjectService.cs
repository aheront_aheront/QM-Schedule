﻿using QM_Schedule.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QM_Schedule.Services
{
    public class SubjectService
    {
        static List<Subject> Subjects { get; }
        static ulong NextId = 3;
        static SubjectService()
        {
            Subjects = new List<Subject>
            {
                new Subject
                {
                    Id = 1,
                    Description = "Very important Subject",
                    Name = "Math"                    
                },
                new Subject
                {
                    Id = 2,
                    Description = "Very boring Subject",
                    Name = "Language"
                }
            };
        }

        public static List<Subject> GetAll() => Subjects;
        public static Subject GetById(ulong Id) =>
            Subjects.FirstOrDefault(s => s.Id == Id);

        public static void Add(Subject subject)
        {
            subject.Id = NextId++;
            Subjects.Add(subject);
        }

        public static void Update(Subject subject)
        {
            var index = Subjects.FindIndex(s => s.Id == subject.Id);
            if (index == -1)
                return;
            Subjects[index] = subject;
        }

        public static void Delete(ulong Id)
        {
            var subject = GetById(Id);
            if (subject is null)
                return;
            Subjects.Remove(subject);
        }
    }
}
