﻿using QM_Schedule.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QM_Schedule.Services
{
    public class GradeService
    {

        static List<Grade> Grades { get; }
        static ulong NextId = 3;
        static GradeService()
        {
            Grades = new List<Grade>
            {
                new Grade
                {
                    Id = 1,
                    Letter = 'D',
                    PupilIds = new List<ulong>{1 ,2, 3}
                },
                new Grade
                {
                    Id = 2,
                    Letter = 'A',
                    PupilIds = new List<ulong>{1,4,2}
                }
            };
        }

        public static List<Grade> GetAll() => Grades;
        public static Grade GetById(ulong Id) =>
            Grades.FirstOrDefault(s => s.Id == Id);

        public static void Add(Grade grade)
        {
            grade.Id = NextId++;
            Grades.Add(grade);
        }

        public static void Update(Grade daySchedule)
        {
            var index = Grades.FindIndex(s => s.Id == daySchedule.Id);
            if (index == -1)
                return;
            Grades[index] = daySchedule;
        }

        public static void Delete(ulong Id)
        {
            var daySchedule = GetById(Id);
            if (daySchedule is null)
                return;
            Grades.Remove(daySchedule);
        }
    }
}
