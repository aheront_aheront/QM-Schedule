﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QM_Schedule.Models
{
    public class Place
    {
        public ulong Id { get; set; }
        public string Name { get; set; }
    }
}
