﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QM_Schedule.Models
{
    public class Lesson
    {
        public ulong Id { get; set; }
        public string Topic { get; set; }
        public ulong PlaceId { get; set; }
        public ulong SubjectId { get; set; }
        public TimeSpan StartTime { get; set; }
        public TimeSpan EndTime { get; set; }
    }
}