﻿using QM_Schedule.Access;
using QM_Schedule.Services;
using QM_Schedule.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QM_Schedule.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class TempLessonController : ControllerBase
    {
        [HttpPost("{UserId}")]
        public IActionResult Create(ulong UserId, TempLesson tempLesson)
        {
            if (!AccessValidate.IsAdmin(UserService.GetById(UserId).AccessLevel))
                return BadRequest();

            TempLessonService.Add(tempLesson);
            return CreatedAtAction(nameof(Create), new { id = tempLesson.Id }, tempLesson);
        }

        [HttpPut("{id}")]
        public IActionResult Update(ulong id, TempLesson tempLesson)
        {
            if (id != tempLesson.Id)
                return BadRequest();

            var existingTempLesson = TempLessonService.GetById(id);
            if (existingTempLesson is null)
                return NotFound();

            TempLessonService.Update(tempLesson);

            return NoContent();
        }
        [HttpDelete("{id}")]
        public IActionResult Delete(ulong id)
        {
            var tempLesson = TempLessonService.GetById(id);

            if (tempLesson is null)
                return NotFound();

            TempLessonService.Delete(id);

            return NoContent();
        }
    }
}
