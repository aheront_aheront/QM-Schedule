﻿using QM_Schedule.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QM_Schedule.Services
{
    public class UserService
    {

        static List<User> Users { get; }
        static ulong NextId = 3;
        static UserService()
        {
            Users = new List<User>
            {
                new User
                {
                    Id = 1,
                    Name = "Alex",
                    Surname = "Strog",
                    Patronymic = "123",
                    PersonalScheduleEventIds = new List<ulong>{ 1},
                    RepeatablePersonalScheduleIds = new List<ulong>{2},
                    AccessLevel = Access.AccessLevel.Admin
                },
                new User
                {
                    Id = 2,
                    Name = "Max",
                    Surname = "Kodan",
                    Patronymic = "jame",
                    PersonalScheduleEventIds = new List<ulong>{2},
                    UnRepeatablePersonalScheduleIds = new List<ulong>{2},
                    AccessLevel = Access.AccessLevel.Pupil
                }
            };
        }

        public static List<User> GetAll() => Users;
        public static User GetById(ulong Id) =>
            Users.FirstOrDefault(s => s.Id == Id);

        public static void Add(User user)
        {
            user.Id = NextId++;
            Users.Add(user);
        }

        public static void Update(User user)
        {
            var index = Users.FindIndex(s => s.Id == user.Id);
            if (index == -1)
                return;
            Users[index] = user;
        }

        public static void Delete(ulong Id)
        {
            var user = GetById(Id);
            if (user is null)
                return;
            Users.Remove(user);
        }
    }
}
