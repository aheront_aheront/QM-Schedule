﻿using QM_Schedule.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QM_Schedule.Services
{
    public class DayScheduleService
    {

        static List<DaySchedule> DaySchedules { get; }
        static ulong NextId = 3;
        static DayScheduleService()
        {
            DaySchedules = new List<DaySchedule>
            {
                new DaySchedule
                {
                    Id = 1,
                    Day = DayOfWeek.Monday,
                    LessonIds = new List<ulong>{ 1, 2}
                },
                new DaySchedule
                {
                    Id = 2,
                    Day = DayOfWeek.Tuesday,
                    LessonIds = new List<ulong> {1,4,2}
                }
            };
        }

        public static List<DaySchedule> GetAll() => DaySchedules;
        public static DaySchedule GetById(ulong Id) =>
            DaySchedules.FirstOrDefault(s => s.Id == Id);

        public static void Add(DaySchedule daySchedule)
        {
            daySchedule.Id = NextId++;
            DaySchedules.Add(daySchedule);
        }

        public static void Update(DaySchedule daySchedule)
        {
            var index = DaySchedules.FindIndex(s => s.Id == daySchedule.Id);
            if (index == -1)
                return;
            DaySchedules[index] = daySchedule;
        }

        public static void Delete(ulong Id)
        {
            var daySchedule = GetById(Id);
            if (daySchedule is null)
                return;
            DaySchedules.Remove(daySchedule);
        }
    }
}
