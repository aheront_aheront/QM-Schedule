﻿using QM_Schedule.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QM_Schedule.Services
{
    public class GradeScheduleService
    {
        static List<GradeSchedule> GradeSchedules { get; }
        static ulong NextId = 3;
        static GradeScheduleService()
        {
            GradeSchedules = new List<GradeSchedule>
            {
                new GradeSchedule
                {
                    Id = 1,
                    DayScheduleIds = new List<ulong>{1,2,4,3},
                    GradeId = 1                   
                },
                new GradeSchedule
                {
                    Id = 2,
                    DayScheduleIds = new List<ulong>{1,4,2,5},
                    GradeId = 2
                }
            };
        }
        
        public static List<GradeSchedule> GetAll() => GradeSchedules;
        public static GradeSchedule GetById(ulong Id) =>
            GradeSchedules.FirstOrDefault(s => s.Id == Id);

        public static void Add(GradeSchedule gradeSchedule)
        {
            gradeSchedule.Id = NextId++;
            GradeSchedules.Add(gradeSchedule);
        }

        public static void Update(GradeSchedule gradeSchedule)
        {
            var index = GradeSchedules.FindIndex(s => s.Id == gradeSchedule.Id);
            if (index == -1)
                return;
            GradeSchedules[index] = gradeSchedule;
        }

        public static void Delete(ulong Id)
        {
            var gradeSchedule = GetById(Id);
            if (gradeSchedule is null)
                return;
            GradeSchedules.Remove(gradeSchedule);
        }
    }
}
