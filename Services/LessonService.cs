﻿using QM_Schedule.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QM_Schedule.Services
{
    public class LessonService
    {

        static List<Lesson> Lessons { get; }
        static ulong NextId = 3;
        static LessonService()
        {
            Lessons = new List<Lesson>
            {
                new Lesson
                {
                    Id = 1,
                    PlaceId = 2,
                    StartTime = TimeSpan.Zero,
                    EndTime = TimeSpan.Zero,
                    Topic = "Ammm ok",
                    SubjectId = 1
                },
                new Lesson
                {
                    Id = 2,
                    PlaceId = 1,
                    StartTime = TimeSpan.Zero,
                    EndTime = TimeSpan.Zero,
                    Topic = "2 Ammmm ok",
                    SubjectId = 2
                }
            };
        }

        public static List<Lesson> GetAll() => Lessons;
        public static Lesson GetById(ulong Id) =>
            Lessons.FirstOrDefault(s => s.Id == Id);

        public static void Add(Lesson lesson)
        {
            lesson.Id = NextId++;
            Lessons.Add(lesson);
        }

        public static void Update(Lesson lesson)
        {
            var index = Lessons.FindIndex(s => s.Id == lesson.Id);
            if (index == -1)
                return;
            Lessons[index] = lesson;
        }

        public static void Delete(ulong Id)
        {
            var lesson = GetById(Id);
            if (lesson is null)
                return;
            Lessons.Remove(lesson);
        }
    }
}
