﻿using QM_Schedule.Access;
using QM_Schedule.Services;
using QM_Schedule.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QM_Schedule.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class LessonController : ControllerBase
    {
        [HttpPost("{UserId}")]
        public IActionResult Create(ulong UserId, Lesson lesson)
        {
            if (!AccessValidate.IsAdmin(UserService.GetById(UserId).AccessLevel))
                return BadRequest();

            LessonService.Add(lesson);
            return CreatedAtAction(nameof(Create), new { id = lesson.Id }, lesson);
        }

        [HttpPut("{id}")]
        public IActionResult Update(ulong id, Lesson lesson)
        {
            if (id != lesson.Id)
                return BadRequest();

            var existingLesson = LessonService.GetById(id);
            if (existingLesson is null)
                return NotFound();

            LessonService.Update(lesson);

            return NoContent();
        }
        [HttpDelete("{id}")]
        public IActionResult Delete(ulong id)
        {
            var lesson = LessonService.GetById(id);

            if (lesson is null)
                return NotFound();

            LessonService.Delete(id);

            return NoContent();
        }
    }
}
