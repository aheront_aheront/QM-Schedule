﻿using QM_Schedule.Access;
using QM_Schedule.Services;
using QM_Schedule.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QM_Schedule.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class GradeController : ControllerBase
    {
        [HttpPost("{UserId}")]
        public IActionResult Create(ulong UserId, Grade grade)
        {
            if (!AccessValidate.IsAdmin(UserService.GetById(UserId).AccessLevel))
                return BadRequest();

            GradeService.Add(grade);
            return CreatedAtAction(nameof(Create), new { id = grade.Id }, grade);
        }

        [HttpPut("{id}")]
        public IActionResult Update(ulong id, Grade grade)
        {
            if (id != grade.Id)
                return BadRequest();

            var existingGrade = GradeService.GetById(id);
            if (existingGrade is null)
                return NotFound();

            GradeService.Update(grade);

            return NoContent();
        }
        [HttpDelete("{id}")]
        public IActionResult Delete(ulong id)
        {
            var grade = GradeService.GetById(id);

            if (grade is null)
                return NotFound();

            GradeService.Delete(id);

            return NoContent();
        }
    }
}
