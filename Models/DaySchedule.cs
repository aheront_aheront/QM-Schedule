﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QM_Schedule.Models
{
    public class DaySchedule
    {
        public ulong Id { get; set; }
        public DayOfWeek Day { get; set; }
        public List<ulong> LessonIds { get; set; }
    }
}
