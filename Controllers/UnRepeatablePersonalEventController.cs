﻿using QM_Schedule.Access;
using QM_Schedule.Services;
using QM_Schedule.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QM_Schedule.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class UnRepeatablePersonalEventController : ControllerBase
    {
        [HttpPost("{UserId}")]
        public IActionResult Create(ulong UserId, UnRepeatablePersonalEvent unRepeatablePersonalEvent)
        {
            if (!AccessValidate.IsAdmin(UserService.GetById(UserId).AccessLevel))
                return BadRequest();

            UnRepeatablePersonalEventService.Add(unRepeatablePersonalEvent);
            return CreatedAtAction(nameof(Create), new { id = unRepeatablePersonalEvent.Id }, unRepeatablePersonalEvent);
        }

        [HttpPut("{id}")]
        public IActionResult Update(ulong id, UnRepeatablePersonalEvent unRepeatablePersonalEvent)
        {
            if (id != unRepeatablePersonalEvent.Id)
                return BadRequest();

            var existingUnRepeatablePersonalEvent = UnRepeatablePersonalEventService.GetById(id);
            if (existingUnRepeatablePersonalEvent is null)
                return NotFound();

            UnRepeatablePersonalEventService.Update(unRepeatablePersonalEvent);

            return NoContent();
        }
        [HttpDelete("{id}")]
        public IActionResult Delete(ulong id)
        {
            var unRepeatablePersonalEvent = UnRepeatablePersonalEventService.GetById(id);

            if (unRepeatablePersonalEvent is null)
                return NotFound();

            UnRepeatablePersonalEventService.Delete(id);

            return NoContent();
        }
    }
}
