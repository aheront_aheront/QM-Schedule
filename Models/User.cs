﻿using System;
using QM_Schedule.Access;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QM_Schedule.Models
{
    public class User
    {
        public ulong Id { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Patronymic { get; set; }
        public List<ulong> RepeatablePersonalScheduleIds { get; set; }
        public List<ulong> UnRepeatablePersonalScheduleIds { get; set; }
        public List<ulong> PersonalScheduleEventIds { get; set; }
        public AccessLevel AccessLevel { get; set; }
    }
}
