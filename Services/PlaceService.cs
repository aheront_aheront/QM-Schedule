﻿using QM_Schedule.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QM_Schedule.Services
{
    public class PlaceService
    {

        static List<Place> Places { get; }
        static ulong NextId = 3;
        static PlaceService()
        {
            Places = new List<Place>
            {
                new Place
                {
                    Id = 1,
                    Name = "Room 210"
                },
                new Place
                {
                    Id = 2,
                    Name = "Room 215"
                }
            };
        }

        public static List<Place> GetAll() => Places;
        public static Place GetById(ulong Id) =>
            Places.FirstOrDefault(s => s.Id == Id);

        public static void Add(Place place)
        {
            place.Id = NextId++;
            Places.Add(place);
        }

        public static void Update(Place place)
        {
            var index = Places.FindIndex(s => s.Id == place.Id);
            if (index == -1)
                return;
            Places[index] = place;
        }

        public static void Delete(ulong Id)
        {
            var place = GetById(Id);
            if (place is null)
                return;
            Places.Remove(place);
        }
    }
}
