﻿using QM_Schedule.Access;
using QM_Schedule.Services;
using QM_Schedule.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QM_Schedule.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class RepeatablePersonalEventController : ControllerBase
    {
        [HttpPost("{UserId}")]
        public IActionResult Create(ulong UserId, RepeatablePersonalEvent repeatablePersonalEvent)
        {
            if (!AccessValidate.IsAdmin(UserService.GetById(UserId).AccessLevel))
                return BadRequest();

            RepeatablePersonalEventService.Add(repeatablePersonalEvent);
            return CreatedAtAction(nameof(Create), new { id = repeatablePersonalEvent.Id }, repeatablePersonalEvent);
        }

        [HttpPut("{id}")]
        public IActionResult Update(ulong id, RepeatablePersonalEvent repeatablePersonalEvent)
        {
            if (id != repeatablePersonalEvent.Id)
                return BadRequest();

            var existingRepeatablePersonalEvent = RepeatablePersonalEventService.GetById(id);
            if (existingRepeatablePersonalEvent is null)
                return NotFound();

            RepeatablePersonalEventService.Update(repeatablePersonalEvent);

            return NoContent();
        }
        [HttpDelete("{id}")]
        public IActionResult Delete(ulong id)
        {
            var repeatablePersonalEvent = RepeatablePersonalEventService.GetById(id);

            if (repeatablePersonalEvent is null)
                return NotFound();

            RepeatablePersonalEventService.Delete(id);

            return NoContent();
        }
    }
}
