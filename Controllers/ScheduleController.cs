﻿using QM_Schedule.Services;
using QM_Schedule.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QM_Schedule.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ScheduleController : ControllerBase
    {
        //get all Schedule
        [HttpGet]
        public ActionResult<Schedule> GetAll() => ScheduleService.GetAll();

        //get Schedule by Grade
        [HttpGet("{day}")]
        public ActionResult<Schedule> GetByUserId(ulong Id)
        {
            return NotFound();


        }


    }
}
