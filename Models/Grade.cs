﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QM_Schedule.Models
{
    public class Grade
    {
        public ulong Id { get; set; }
        public byte Digit { get; set; }
        public char Letter { get; set; }
        public List<ulong> PupilIds{ get; set; }
    }
}
